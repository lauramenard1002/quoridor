
#Programme Python QUORIDOR
import numpy as np
import pylab

#Définition du plateau initial
plateau=np.zeros((17,17))
for i in range(0,17,2):
    for j in range(0,17,2):
        plateau[i,j]=1
plateau[0,8]=3
plateau[16,8]=2
print(plateau)

def position (J):
    for i in range(0,17):
        for j in range(0,17):
            if plateau[i,j]==J:
                return(i,j)

def mouvement(J):
    deplacement=False
    while deplacement==False:
        deplacement=True
        z=int(input("Choisir de monter (-2), de descendre (2), d'aller à droite (1) ou d'aller à gauche (-1) :"))
        a,b=position(J)
        if abs(z)==2:
            A=a+z
            B=b
            if A>=17 or A<0:
                deplacement=False
                print('déplacement incorrect, rejouer')
        else:
            A=a
            B=b+2*z
            if B>=17 or B<0:
                deplacement=False
                print('déplacement incorrect, rejouer')
    plateau[a,b]=1
    plateau[A,B]=J
    return(plateau)

def mouvement2(J):
    deplacement=False
    while deplacement==False:
        deplacement=True
        z=int(input("Choisir de monter (-2), de descendre (2), d'aller à droite (1), d'aller à gauche (-1) ou de mettre une barrière (k) :"))
        a,b=position(J)
        if abs(z)==2:
            A=a+z
            B=b
            if A>=17 or A<0:
                deplacement=False
                print('déplacement incorrect, rejouer')
        else:
            A=a
            B=b+2*z
            if B>=17 or B<0:
                deplacement=False
                print('déplacement incorrect, rejouer')
    plateau[a,b]=1
    plateau[A,B]=J
    return(plateau)

def barriere():
    print('vous ne pouvez placer que des barrières dans les cases noires, une barrière bloque 2 cases rouges')
    d,e=int(input("1ère extrémité de la barrière: ligne :")),int(input("1ère extrémité de la barrière: colonne :"))
    f,g=int(input("2ème extrémité de la barrière: ligne :")),int(input("2ème extrémité de la barrière: colonne :"))
    plateau[d,e]=4
    plateau[f,g]=4

def barriere2():
    print('vous ne pouvez placer que des barrières dans les cases noires, une barrière bloque 2 cases rouges')
    x,y,z=int(input("1ère extrémité de la barrière: ligne :")),int(input("1ère extrémité de la barrière: colonne :")),int(input("Choisir la direction de la barrière haut (-2), bas (2), droite (1), gauche (-1) :"))
    if 0<=x<len(plateau) and 0<=y<len(plateau):
        print(x,y)
        if plateau[x,y]==0:
            if z==-2 and x-2>0 and plateau[x-1,y]==0 and plateau[x-2,y]==0:
                plateau[x,y]=4
                plateau[x-1,y]=4
                plateau[x-2,y]=4
                return(True)

            if z==2 and x+2<len(plateau) and plateau[x+1,y]==0 and plateau[x+2,y]==0:
                plateau[x,y]=4
                plateau[x+1,y]=4
                plateau[x+2,y]=4
                return(True)

            if z==1 and y+2<len(plateau) and plateau[x,y+1]==0 and plateau[x,y+2]==0:
                plateau[x,y]=4
                plateau[x,y+1]=4
                plateau[x,y+2]=4
                return(True)

            if z==-1 and y-2>0 and plateau[x,y-1]==0 and plateau[x,y-2]==0:
                plateau[x,y]=4
                plateau[x,y-1]=4
                plateau[x,y-2]=4
                return(True)
    return(False)






#Affichage du plateau
def vue(plateau):
    A=np.zeros((17,17,3))
    for i in range(0,17):
        for j in range(0,17):
            if plateau[i,j]==0:
                A[i,j,0]=0
                A[i,j,1]=0
                A[i,j,2]=0
            elif plateau[i,j]==1:
                A[i,j,0]=1
                A[i,j,1]=0
                A[i,j,2]=1/10
            elif plateau[i,j]==2:
                A[i,j,0]=0
                A[i,j,1]=1
                A[i,j,2]=1
            elif plateau[i,j]==3:
                A[i,j,0]=0
                A[i,j,1]=1
                A[i,j,2]=1/10
            else:
                A[i,j,0]=1
                A[i,j,1]=1/2
                A[i,j,2]=0
    pylab.imshow(A,interpolation='none')
    pylab.axis('off')
    pylab.show()

#Vérification de la condition finale
def fini (plateau):
    y=3
    x=2
    for f in range (0,16,2):
            if y==plateau[16,f]:
                return(True)
    for f in range (0,16,2):
            if x==plateau[0,f]:
                return(True)
            else:
                return(False)







